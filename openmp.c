#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "common.h"

omp_lock_t mutexes[NUMBER_OF_MOVIES];


/*
*
* Protótipos das funções
*
* */

void init_parallel();

void destroy_parallel();

void calculate_movies_similarity_parallel(int thread_count);

void calc_similarity_parallel(unsigned int movie_a_id, unsigned int movie_b_id);

void show_similar_items_parallel(int thread_count);

static double (*sim_func)(unsigned int, unsigned int);


/*
*
*  Implementações das funções
*
**/


/*
* Inicializa as estruturas de dados para o algoritmo paralelo
**/
void init_parallel() {
    unsigned int i, j, k;

    for (i = 0; i < NUMBER_OF_MOVIES; i++) {
        sim[i] = NULL;
        omp_init_lock(&mutexes[i]); // inicializa os mutexed
        for (k = 0; k < NUMBER_OF_MOVIES; k++) {
            already_calculated_similarity[i][k] = 0;
        }
        for (j = 0; j < NUMBER_OF_USERS; j++) ratings[i][j] = -1;
    }
    printf("\nEstruturas de dados inicializadas\n");
    fflush(stdout);
}


/*
* Para cada filme, destrói a árvore de similaridades, liberando a memória, e detrói o mutex
**/
void destroy_parallel() {
    int i;
    for (i = 0; i < NUMBER_OF_MOVIES; i++) {
        omp_destroy_lock(&mutexes[i]);
        destroy_similarity_tree(sim[i]);
    }
}

/*
* Algoritmo paralelo para calcular similaridade entre os filmes utilizando openMP
**/
void calculate_movies_similarity_parallel(int thread_count) {

    unsigned int movie_a_id;
    unsigned int movie_b_id;
    unsigned int user_id;

    /*
    * Foi feita a experiência de usar o | #pragma omp parallel | fora desta função,
    * e dentro da função somente  | #pragma omp for |, mas obtivêmos uma performance
    * ligeiramente melhor utilizando | #pragma omp parallel for |  dentro da função,
    * como é utilizada abaixo
    * */
    #pragma omp parallel for private(movie_a_id, movie_b_id, user_id) num_threads(thread_count)
    for (movie_a_id = 0; movie_a_id < NUMBER_OF_MOVIES; movie_a_id++) {
        for (user_id = 0; user_id < NUMBER_OF_USERS; user_id++) {
            if (ratings[movie_a_id][user_id] != -1) {
                for (movie_b_id = 0; movie_b_id < NUMBER_OF_MOVIES; movie_b_id++) {
                    if ((ratings[movie_b_id][user_id] != -1) && (movie_b_id != movie_a_id)) {
                        calc_similarity_parallel(movie_a_id, movie_b_id);
                    }
                }
            }
        }
    }
}

/*
* Calcula a similaridade entre dois filmes, protegendo as regiões críticas, e portanto
* para ser usado com múltiplas threads
**/
void calc_similarity_parallel(unsigned int movie_a_id, unsigned int movie_b_id) {

    int go_ahead = 0;

    omp_lock_t* mutex = movie_a_id > movie_b_id ? (&mutexes[movie_a_id]): (&mutexes[movie_b_id]);


    omp_set_lock(mutex);
    if (already_calculated_similarity[movie_a_id][movie_b_id] == 0) {
        go_ahead = 1;
        already_calculated_similarity[movie_a_id][movie_b_id] = 1;
        already_calculated_similarity[movie_b_id][movie_a_id] = 1;
    }
    omp_unset_lock(mutex);

    if (go_ahead == 1) {

        double similarity = sim_func(movie_a_id, movie_b_id);

        omp_set_lock(mutex);
        sim[movie_a_id] = insert_similar_movie_node(sim[movie_a_id], movie_b_id, similarity);
        sim[movie_b_id] = insert_similar_movie_node(sim[movie_b_id], movie_a_id, similarity);
        omp_unset_lock(mutex);
    }
}

/*
* Calcula a similaridade dos filmes utilizando o algoritmo paralelo com OpenMP
**/
void show_similar_items_parallel(int thread_count) {
    printf("\nCalculando a similaridade entre %d filmes com OpenMP, utilizando %d threads.\n", NUMBER_OF_MOVIES, thread_count);
    fflush(stdout);

    init_parallel();
    read_movie_names();
    read_ratings_file();

    tic();
    calculate_movies_similarity_parallel(thread_count);
    toc();

    show_similar_movies(6);
    destroy_parallel();
}

int main(int argc, const char *argv[]) {

    if (argc < 2) {
        printf("\nO número de threads deve ser informado\n");
    } else {
//        sim_func = &sim_pearson;
        sim_func = &sim_euclidean_dist;
        int thread_count = (int) strtol(argv[1], NULL, 10);
        show_similar_items_parallel(thread_count);
    }

    return 0;
}
