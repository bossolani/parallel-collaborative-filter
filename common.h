#ifndef COMMON_H
#define  COMMON_H


#define NUMBER_OF_MOVIES 1682
#define NUMBER_OF_USERS 943

/*
* Os filmes similares precisam ser adicinados dinamicamente e precisam ser facilmente ordenados.
* Por isso são armazenados em uma árvore binária.
* */
typedef struct _similar_movie_node {
    unsigned int movie_id;
    double similarity;
    struct _similar_movie_node *left;
    struct _similar_movie_node *right;
    int n;
} similar_movie_node;

similar_movie_node *sim[NUMBER_OF_MOVIES]; // uma árvore para cada filme

// Avaliações de filmes por usuários. Esse array tende a ser esparso.
double ratings[NUMBER_OF_MOVIES][NUMBER_OF_USERS];

char *movie_names[NUMBER_OF_MOVIES];

// Pares de filmes cuja similaridade já foi calculada
unsigned char already_calculated_similarity[NUMBER_OF_MOVIES][NUMBER_OF_MOVIES];


void read_movie_names();

void read_ratings_file();

void show_similar_movies(unsigned int movie_id);

similar_movie_node *create_similar_movie_node(unsigned int movie_id, double similarity);

similar_movie_node *insert_similar_movie_node(similar_movie_node *node, unsigned int movie_id, double similarity);

void destroy_similarity_tree(similar_movie_node *node);

void traverse_similar_movies(similar_movie_node *node, void (*visit)(unsigned int, double));

void tic();

double toc();

double sim_euclidean_dist(unsigned int movie_a_id, unsigned int movie_b_id);
double sim_pearson(unsigned int movie_a_id, unsigned int movie_b_id);

#endif