#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "common.h"

pthread_mutex_t mutexes[NUMBER_OF_MOVIES];


/*
*
* Protótipos das funções
*
* */

void parallel(void *(*func)(void *));

void init_parallel();

void destroy_parallel();

void show_similar_items_parallel();

void *calculate_movies_similarity_parallel(void *thread);

void calc_similarity_parallel(unsigned int movie_a_id, unsigned int movie_b_id);

static int thread_count;

static double (*sim_func)(unsigned int, unsigned int);


/*
* Implementações das funções
*
* */

/*
* Inicializa as estruturas de dados para o algoritmo paralelo
**/
void init_parallel() {
    unsigned int i, j, k;

    for (i = 0; i < NUMBER_OF_MOVIES; i++) {
        sim[i] = NULL;
        pthread_mutex_init(&mutexes[i], NULL);
        for (k = 0; k < NUMBER_OF_MOVIES; k++) {
            already_calculated_similarity[i][k] = 0;
        }
        for (j = 0; j < NUMBER_OF_USERS; j++) ratings[i][j] = -1;
    }
    printf("\nEstruturas de dados inicializadas\n");
    fflush(stdout);
}

/*
* Para cada filme, destrói a árvore de similaridades, liberando a memória, e detrói o mutex
**/
void destroy_parallel() {
    int i;
    for (i = 0; i < NUMBER_OF_MOVIES; i++) {
        pthread_mutex_destroy(&mutexes[i]);
        destroy_similarity_tree(sim[i]);
    }
}


/*
* Algoritmo paralelo para calcular similaridade entre os filmes utilizando PThread
**/
void *calculate_movies_similarity_parallel(void *thread) {

    unsigned int my_number_of_movies = NUMBER_OF_MOVIES / thread_count;
    unsigned int my_begin = ((unsigned int) thread) * my_number_of_movies;
    unsigned int my_end = my_begin + my_number_of_movies;

    unsigned int movie_a_id;
    unsigned int movie_b_id;
    unsigned int user_id;

    for (movie_a_id = my_begin; movie_a_id < my_end; movie_a_id++) {
        for (user_id = 0; user_id < NUMBER_OF_USERS; user_id++) {
            if (ratings[movie_a_id][user_id] != -1) {
                for (movie_b_id = 0; movie_b_id < NUMBER_OF_MOVIES; movie_b_id++) {
                    if ((ratings[movie_b_id][user_id] != -1) && (movie_b_id != movie_a_id)) {
                        calc_similarity_parallel(movie_a_id, movie_b_id);
                    }
                }
            }
        }
    }
    return NULL;
}

void calc_similarity_parallel(unsigned int movie_a_id, unsigned int movie_b_id) {

    int go_ahead = 0;

    pthread_mutex_t* mutex = movie_a_id > movie_b_id ? (&mutexes[movie_a_id]): (&mutexes[movie_b_id]);

    pthread_mutex_lock(mutex);

    if (already_calculated_similarity[movie_a_id][movie_b_id] == 0) {
        go_ahead = 1;
        already_calculated_similarity[movie_a_id][movie_b_id] = 1;
        already_calculated_similarity[movie_b_id][movie_a_id] = 1;
    }

    pthread_mutex_unlock(mutex);

    if (go_ahead == 1) {

        double similarity = sim_func(movie_a_id, movie_b_id);

        pthread_mutex_lock(mutex);
        sim[movie_a_id] = insert_similar_movie_node(sim[movie_a_id], movie_b_id, similarity);
        sim[movie_b_id] = insert_similar_movie_node(sim[movie_b_id], movie_a_id, similarity);
        pthread_mutex_unlock(mutex);
    }
}

void show_similar_items_parallel() {
    printf("\nCalculando a similaridade entre %d filmes com PThread, utilizando %d threads.\n", NUMBER_OF_MOVIES, thread_count);
    fflush(stdout);

    init_parallel();
    read_movie_names();
    read_ratings_file();

    tic();
    parallel(calculate_movies_similarity_parallel);
    toc();

    show_similar_movies(6);
    destroy_parallel();
}

void parallel(void *(*func)(void *)) {
    long thread;
    pthread_t *thread_handles = malloc(thread_count * sizeof(pthread_t));
    for (thread = 0; thread < thread_count; thread++) pthread_create(&thread_handles[thread], NULL, func, (void *) thread);
    for (thread = 0; thread < thread_count; thread++) pthread_join(thread_handles[thread], NULL);
    free(thread_handles);
}


int main(int argc, const char *argv[]) {

    if (argc < 2) {
        printf("\nO número de threads deve ser informado\n");
    } else {
//        sim_func = &sim_pearson;
        sim_func = &sim_euclidean_dist;

        thread_count = (int) strtol(argv[1], NULL, 10);
        show_similar_items_parallel();
    }

    return 0;
}
