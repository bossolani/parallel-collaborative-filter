#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include <math.h>


#ifdef _WIN32
#include <Windows.h>
#else

#include <sys/time.h>

#endif


// Limita a quantidade de filmes similares que será mostrada
static unsigned short int similar_count_to_display = 10;

/*
* Implementação de um "relógio de parede", para medir o tempo real de execução
**/
static double now;

#ifdef _WIN32
double get_wall_time() {
    LARGE_INTEGER time,freq;
    QueryPerformanceFrequency(&freq);
    QueryPerformanceCounter(&time);
    return (double)time.QuadPart / freq.QuadPart;
}
#else

double get_wall_time() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return t.tv_sec + t.tv_usec / 1000000.0;
}

#endif

void tic() {
    now = get_wall_time();
}

double toc() {
    double elapsed = get_wall_time() - now;
    printf("\nTempo decorrido = %f segundos\n", elapsed);
    return elapsed;
}

static void print_similar_movie(unsigned int movie_id, double similarity);

/*
* Cria um nó que será inserido na árvore binária de filmes similares
**/
similar_movie_node *create_similar_movie_node(unsigned int movie_id, double similarity) {
    similar_movie_node *x = malloc(sizeof(similar_movie_node));
    x->movie_id = movie_id;
    x->similarity = similarity;
    x->left = NULL;
    x->right = NULL;
    x->n = 0;
    return x;
}

/*
* Insere filme similar na árvore de filmes similares do filme do filme correspondente ao move_id
**/
similar_movie_node *insert_similar_movie_node(similar_movie_node *node, unsigned int movie_id, double similarity) {
    if (node == NULL) return create_similar_movie_node(movie_id, similarity);

    if (similarity < node->similarity)
        node->left = insert_similar_movie_node(node->left, movie_id, similarity);
    else
        node->right = insert_similar_movie_node(node->right, movie_id, similarity);

    (node->n)++;

    return node;
}

/*
* Destrói a árvore de filmes similares, liberando a memória
*/
void destroy_similarity_tree(similar_movie_node *node) {
    if (node != NULL) {
        destroy_similarity_tree(node->left);
        destroy_similarity_tree(node->right);
        free(node);
    }
}

/*
* Visita cada nó da árvore de similaridade e chama a função visit() passando id e similaridade como parâmetros
* Os nós são visitados na ordem de similaridade.
* limita o número de similaridades visitadas ao valor de similar_count_to_display
**/
void traverse_similar_movies(similar_movie_node *node, void (*visit)(unsigned int, double)) {
    if (node == NULL || similar_count_to_display <= 0) return;
    traverse_similar_movies(node->right, visit);
    if (similar_count_to_display) {
        visit(node->movie_id, node->similarity);
        similar_count_to_display--;
        traverse_similar_movies(node->left, visit);
    }
}

/*
* Carrega os nomes dos filmes do arquivo e armazena em um array
**/
void read_movie_names() {

    unsigned int movie_id;
    char buffer[512];
    char *token, *line;

    FILE *file = fopen("u.item", "r");

    if (file == NULL) {
        printf("\nErro ao abrir arquivo\n");
        exit(1);
    }

    while ((line = fgets(buffer, sizeof(buffer), file)) != NULL) {
        token = strtok(line, "|");
        movie_id = atol(token) - 1; //zero based
        token = strtok(NULL, "|");
        movie_names[movie_id] = malloc(strlen(token) + 1);
        strcpy(movie_names[movie_id], token);
    }

//    printf("\nNomes dos filmes carregados\n");
//    fflush(stdout);

    fclose(file);
}

void read_ratings_file() {
    unsigned int user_id;
    unsigned int movie_id;
    double rating;
    char buffer[1024];
    char *token, *line;


    FILE *file = fopen("u.data", "r");

    if (file == NULL) {
        printf("\nErro ao abrir arquivo\n");
        exit(1);
    }

    while ((line = fgets(buffer, sizeof(buffer), file)) != NULL) {
        token = strtok(line, "\t");
        user_id = atol(token) - 1; // zero based!
        token = strtok(NULL, "\t");
        movie_id = atol(token) - 1; // zero based!
        token = strtok(NULL, "\t");
        rating = atof(token);

        ratings[movie_id][user_id] = rating;
    }

//    printf("\nAvaliações carregadas\n");
//    fflush(stdout);

    fclose(file);
}


/*
* Calcula a similaridade utilizando distância Euclidiana.
* Valores variam de 0 a 1
* */
double sim_euclidean_dist(unsigned int movie_a_id, unsigned int movie_b_id) {
    unsigned int user_id;
    double sum_of_squares = 0;
    for (user_id = 0; user_id < NUMBER_OF_USERS; user_id++) {
        if (ratings[movie_a_id][user_id] != -1 && ratings[movie_b_id][user_id] != -1)
            sum_of_squares += pow(ratings[movie_a_id][user_id] - ratings[movie_b_id][user_id], 2);
    }
    return 1 / (1 + sqrt(sum_of_squares));
}

/*
* Calcula a similaridade utilizando o coeficiente de Pearson
* */
double sim_pearson(unsigned int movie_a_id, unsigned int movie_b_id) {
    unsigned int user_id;

    double sum_movie_a = 0;
    double sum_movie_a_squared = 0;
    double sum_movie_b = 0;
    double sum_movie_b_squared = 0;
    double product_sum = 0;
    int n = 0;

    for (user_id = 0; user_id < NUMBER_OF_USERS; user_id++) {
        if (ratings[movie_a_id][user_id] != -1 && ratings[movie_b_id][user_id] != -1) {
            sum_movie_a += ratings[movie_a_id][user_id];
            sum_movie_a_squared += pow(ratings[movie_a_id][user_id], 2);
            sum_movie_b += ratings[movie_b_id][user_id];
            sum_movie_b_squared += pow(ratings[movie_b_id][user_id], 2);
            product_sum += ratings[movie_a_id][user_id] * ratings[movie_b_id][user_id];
            n++;
        }
    }

//    double num = product_sum - (sum_movie_a * sum_movie_b / n);
//    double den = sqrt((sum_movie_a_squared - pow(sum_movie_a, 2) / n) * (sum_movie_b_squared - pow(sum_movie_b, 2) / n));

    double num = n*product_sum - (sum_movie_a * sum_movie_b);
    double den = sqrt((n * sum_movie_a_squared - pow(sum_movie_a, 2)) * (n * sum_movie_b_squared - pow(sum_movie_b, 2)));

    if (den == 0) return 0;

    return num / den;
}


/*
* Mostra os filmes similares.
**/

void show_similar_movies(unsigned int movie_id) {
    printf("\nFilmes similares ao filme \"%s\": \n\n", movie_names[movie_id]);
    similar_count_to_display = 10;
    traverse_similar_movies(sim[movie_id], print_similar_movie);
}

static void print_similar_movie(unsigned int movie_id, double similarity) {
    printf("- %s: %f\n", movie_names[movie_id], similarity);
}

