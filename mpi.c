/*
* Este arquivo correspode à versão 2 que adotamos como definitiva.
* Desta versão que colhemos os resultados
*
* para compilar: mpicc -g -Wall -std=C99 -o mpi mpi.c common.c
* para executar: mpiexec -n num ./mpi
*
* */

#include <mpi.h>
#include "common.h"

int process_count;
int my_process;

/*
*
* Protótipos das funções
*
* */

void receive_result();

void calc_sim();

void do_calc_sim(unsigned int movie_a_id, unsigned int movie_b_id);

static double (*sim_func)(unsigned int, unsigned int);


/*
* Implementações das funções
*
* */
void receive_result() {

    unsigned int i;
    for (i = 0; i < NUMBER_OF_MOVIES; i++) sim[i] = NULL;

    int alive_workers = process_count - 1;

    int received = 0;

    while (alive_workers > 0) {
        double response[3];
        MPI_Recv(&response[0], 3, MPI_DOUBLE, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        if (response[0] < 0) {

            alive_workers--;
        }
        else {
            received++;

            unsigned int movie_a_id = (unsigned int) response[0];
            unsigned int movie_b_id = (unsigned int) response[1];
            double similarity = response[2];
            sim[movie_a_id] = insert_similar_movie_node(sim[movie_a_id], movie_b_id, similarity);
            sim[movie_b_id] = insert_similar_movie_node(sim[movie_b_id], movie_a_id, similarity);
        }
    }

    read_movie_names();
    show_similar_movies(6);

    for (i = 0; i < NUMBER_OF_MOVIES; i++) destroy_similarity_tree(sim[i]);

}

void calc_sim() {

    unsigned int i, j;
    for (i = 0; i < NUMBER_OF_MOVIES; i++)
        for (j = 0; j < NUMBER_OF_USERS; j++) ratings[i][j] = -1;

    read_ratings_file();

    unsigned int worker_process_count = (unsigned int) (process_count - 1);
    unsigned int my_worker_process = (unsigned int) (my_process - 1);
    unsigned int my_number_of_rows = NUMBER_OF_MOVIES / (2 * worker_process_count);
    unsigned int my_row_begin = my_worker_process * my_number_of_rows;
    unsigned int my_row_end = my_row_begin + my_number_of_rows;
    unsigned int movie_a_id;
    unsigned int user_id;


    /*
        Similaridade é uma relação reflexiva, a de (A,B) é a mesma de (B,A).
        Para evitar calcular duas vezes teriamos que saber qual já foi calculada,
        o que é difícil com memória distribuída.
        Chegamos ao algorítmo abaixo, que evita repetição e faz o balancing entre os
        processos ao mesmo tempo.

        A iteração dos filmes é ilustrada abaixo. Cada processo que processa uma linha X,
        processa também a linha complementária, por exemplo 0 e 5, para que o número de filmes
        comparados seja sempre o mesmo para todos os processos.


       | 0 | 1 | 2 | 3 | 4 | 5 | 6 | ...
       -----------------------------
    0  |   | * | * | * | * | * | * |
       -----------------------------
    1  |   |   | * | * | * | * | * |
       -----------------------------
    2  |   |   |   | * | * | * | * |
       -----------------------------
    3  |   |   |   |   | * | * | * |
       -----------------------------
    4  |   |   |   |   |   | * | * |
       -----------------------------
    5  |   |   |   |   |   |   | * |
       -----------------------------
    6  |   |   |   |   |   |   |   |
       -----------------------------
    .
    .
    .

    */

    for (movie_a_id = my_row_begin; movie_a_id < my_row_end; movie_a_id++) {

        unsigned int movie_b_id;

        for (movie_b_id = movie_a_id + 1; movie_b_id < NUMBER_OF_MOVIES; movie_b_id++) {
            int found = 0;
            for (user_id = 0; !found && (user_id < NUMBER_OF_USERS); user_id++) {
                if ((ratings[movie_a_id][user_id] != -1) && (ratings[movie_b_id][user_id] != -1)) {
                    do_calc_sim(movie_a_id, movie_b_id);
                    found = 1;
                }
            }
        }

        unsigned int complementary_row = (my_number_of_rows * worker_process_count * 2)  - movie_a_id -1;

        for (movie_b_id = complementary_row + 1; movie_b_id < NUMBER_OF_MOVIES; movie_b_id++) {
            int found = 0;
            for (user_id = 0; !found && (user_id < NUMBER_OF_USERS); user_id++) {
                if ((ratings[complementary_row][user_id] != -1) && (ratings[movie_b_id][user_id] != -1)) {
                    do_calc_sim(complementary_row, movie_b_id);
                    found = 1;
                }
            }
        }
    }

    // Se sobrou algum, o último processo calcula.
    if(my_worker_process == (worker_process_count-1)) {
        unsigned int last_row =  (my_number_of_rows * worker_process_count * 2) - 1;
        for (movie_a_id = last_row+1; movie_a_id < NUMBER_OF_MOVIES; movie_a_id++) {
            unsigned int movie_b_id;
            for (movie_b_id = movie_a_id+1; movie_b_id < NUMBER_OF_MOVIES; movie_b_id++) {
                int found = 0;
                for (user_id = 0; !found && (user_id < NUMBER_OF_USERS); user_id++) {
                    if ((ratings[movie_a_id][user_id] != -1) && (ratings[movie_b_id][user_id] != -1)) {
                        do_calc_sim(movie_a_id, movie_b_id);
                        found = 1;
                    }
                }
            }
        }
    }

    // Avisa processo 0 que que este processo já acabou de calcular
    double response[3];
    response[0] = -1;
    MPI_Send(&response[0], 3, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
}

void do_calc_sim(unsigned int movie_a_id, unsigned int movie_b_id) {
    double response[3];
    response[0] = movie_a_id;
    response[1] = movie_b_id;
    response[2] = sim_func(movie_a_id, movie_b_id);
    already_calculated_similarity[movie_a_id][movie_b_id] = 1;
    already_calculated_similarity[movie_b_id][movie_a_id] = 1;
    MPI_Send(&response[0], 3, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
}

int main(int argc, const char *argv[]) {

    sim_func = &sim_euclidean_dist;

    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &process_count);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_process);

    if (my_process == 0) {
        tic();
        receive_result();
        toc();
    } else {
        calc_sim();
    }

    MPI_Finalize();
    return 0;
}
