#include <stdio.h>
#include <math.h>
#include "common.h"

/*
*
* Protótipos das funções
*
* */

void init();

void destroy();

void calculate_movies_similarity();

void calc_similarity(unsigned int movie_a_id, unsigned int movie_b_id);

void show_similar_items();

static double (*sim_func)(unsigned int, unsigned int);

/*
*
*  Implementações das funções
*
**/

/*
* Inicializa as estruturas de dados para o algoritmo serial
**/
void init() {
    int i, j, k;
    for (i = 0; i < NUMBER_OF_MOVIES; i++) {
        sim[i] = NULL;
        for (k = 0; k < NUMBER_OF_MOVIES; k++) already_calculated_similarity[i][k] = 0;
        for (j = 0; j < NUMBER_OF_USERS; j++) ratings[i][j] = -1;
    }
    printf("\nEstruturas de dados inicializadas\n");
    fflush(stdout);
}

/*
* Para cada filme, destrói a árvore de similaridades, liberando a memória
**/
void destroy() {
    int i;
    for (i = 0; i < NUMBER_OF_MOVIES; i++) destroy_similarity_tree(sim[i]);
}

int count=0;

/*
* Algoritmo serial para calcular similaridade entre os filmes
**/
void calculate_movies_similarity() {

    unsigned int movie_a_id;
    unsigned int movie_b_id;
    unsigned int user_id;


    for (movie_a_id = 0; movie_a_id < NUMBER_OF_MOVIES; movie_a_id++) {
        for (user_id = 0; user_id < NUMBER_OF_USERS; user_id++) {
            if (ratings[movie_a_id][user_id] != -1) {
                for (movie_b_id = 0; movie_b_id < NUMBER_OF_MOVIES; movie_b_id++) {
                    if ((ratings[movie_b_id][user_id] != -1) && (movie_b_id != movie_a_id)) {
                        if(movie_a_id != movie_b_id) {
                            calc_similarity(movie_a_id, movie_b_id);
                        }
                    }
                }
            }
        }
    }

    printf("calculadas %d similaridades", count);
}

/*
* Calcula a similaridade entre dois filmes. Não é thread safe. Utilizado pelo algoritmo serial
**/
void calc_similarity(unsigned int movie_a_id, unsigned int movie_b_id) {
    if ((already_calculated_similarity[movie_a_id][movie_b_id] == 0) && (already_calculated_similarity[movie_b_id][movie_a_id] == 0)) {
        unsigned int user_id;
        double sum_of_squares = 0;
        for (user_id = 0; user_id < NUMBER_OF_USERS; user_id++) {
            if (ratings[movie_a_id][user_id] != -1 && ratings[movie_b_id][user_id] != -1)
                sum_of_squares += pow(ratings[movie_a_id][user_id] - ratings[movie_b_id][user_id], 2);
        }

        double similarity = sim_func(movie_a_id, movie_b_id);

        already_calculated_similarity[movie_a_id][movie_b_id] = 1;
        already_calculated_similarity[movie_b_id][movie_a_id] = 1;
        sim[movie_a_id] = insert_similar_movie_node(sim[movie_a_id], movie_b_id, similarity);
        sim[movie_b_id] = insert_similar_movie_node(sim[movie_b_id], movie_a_id, similarity);

        count++;

    }
}

/*
* Calcula a similaridade dos filmes utilizando o algoritmo serial
**/
void show_similar_items() {
    printf("\nCalculando a similaridade entre %d filmes de maneira SERIAL\n", NUMBER_OF_MOVIES);
    fflush(stdout);

    init();
    read_movie_names();
    read_ratings_file();

    printf("\nCalculando similaridade entre os filmes...\n");
    fflush(stdout);

    tic();
    calculate_movies_similarity();
    toc();
    show_similar_movies(6);
    destroy();
}

int main(int argc, const char *argv[]) {
//    sim_func = &sim_pearson;
    sim_func = &sim_euclidean_dist;

    show_similar_items();
    return 0;
}
