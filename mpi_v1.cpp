/*
* Esse aquivo corresponde à primeira tentativa de implementação do MPI
*
* Estamos enviando como parte do histórico da nossa evolução, mas não
* deve ser considerado para comparação da performance.
*
*
* */
#include <omp.h>
#include <iostream>
#include <map>
#include <set>
#include <fstream>
#include <sstream>
#include <mpi.h>
#include <math.h>
#include <vector>
#include <algorithm>
#include <sys/time.h>

using namespace std;

map<int, string> movie_names;
map<int, map<int, double> > ratings_by_movie_id;
map<int, map<int, double> > ratings_by_user_id;
map<int, map<int, double> > similarities;
set<pair<int, int> > process_movie_id_sent;
map<int, map<int, double> > my_ratings;

set<pair<int, int> > similarity_task_sent;

struct movie_ratings {
    int number_of_ratings;
    int *user_ids;
    double *ratings;
};
typedef struct movie_ratings movie_ratings_t;

map<int, movie_ratings_t*> struct_ratings_for_movie;

bool all_sent = false;

int similarities_received;

int similarities_sent;

int process_count;

int my_process;

void load_movie_names();

void load_ratings();

void receive_ratings();

void receive_calc_sim();

movie_ratings_t * get_structure_rating_for_movie(int movie_id);

double calc_sim(int movie_a_id, int movie_b_id);

void distribute_tasks();

void receive_result();

void go();

void main_process();

void worker_process();

void do_send_calc_similarity(int movie_a_id, int movie_b_id, int current_process);

void send_movie_ratings(int movie_id, int current_process);

void main_process_destroy();

void send_calc_similarity(int movie_a_id, int movie_b_id, int &current_process);

void show_similar_movies(int movie_id);

void kill_workers();

double get_wall_time();
void tic();
double toc();

static double now;

double get_wall_time() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return t.tv_sec + t.tv_usec / 1000000.0;
}

void tic() {
    now = get_wall_time();
}

double toc() {
    double elapsed = get_wall_time() - now;
    printf("\nTempo decorrido = %f segundos\n", elapsed);
    return elapsed;
}

void load_movie_names() {
    ifstream fin("u.item");
    while (fin) {
        string line;
        if (!getline(fin, line)) break;
        stringstream str_stream(line);
        string token;
        getline(str_stream, token, '|');
        int id = atoi(token.c_str());
        getline(str_stream, token, '|');
        movie_names[id] = token;
    }
    fin.close();
    cout << "Nomes dos filmes carregados" << endl;
}

void load_ratings() {
    ifstream fin("u.data");
    while (fin) {
        string line;
        if (!getline(fin, line)) break;
        stringstream str_stream(line);
        string token;
        getline(str_stream, token, '\t');
        int user_id = atoi(token.c_str());
        getline(str_stream, token, '\t');
        int movie_id = atoi(token.c_str());
        getline(str_stream, token, '\t');
        double rating = atoi(token.c_str());
        ratings_by_movie_id[movie_id][user_id] = rating;
        ratings_by_user_id[user_id][movie_id] = rating;
    }
    fin.close();
    cout << "Avaliações carregadas" << endl;
}


void go() {

    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &process_count);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_process);

    if (my_process == 0) {
        tic();
        main_process();
        toc();
    } else {
        worker_process();
    }

    MPI_Finalize();
}

/*
*
* MAIN mpPROCESS
*
* */

void main_process() {

#pragma omp parallel num_threads(2)
    {
        int my_thread = omp_get_thread_num();

        if(my_thread == 0) {
            load_movie_names();
            load_ratings();
            distribute_tasks();
        }

        if(my_thread == 1) {
            receive_result();
        }
    }

    show_similar_movies(7);
    main_process_destroy();
}

void distribute_tasks() {
    all_sent = false;
    int current_process = 1;
    for (map<int, map<int, double> >::iterator movie_a_iter = ratings_by_movie_id.begin(); movie_a_iter != ratings_by_movie_id.end(); movie_a_iter++) { // todos os filmes

        int movie_a_id = movie_a_iter->first;
        map<int, double> &movie_a_user_ratings = movie_a_iter->second;

        for (map<int, double>::iterator movie_a_user_ratings_iter = movie_a_user_ratings.begin(); movie_a_user_ratings_iter != movie_a_user_ratings.end(); movie_a_user_ratings_iter++) { // todos os ratings de usuário daquele filme

            int user_id = movie_a_user_ratings_iter->first;
            map<int, double> &this_user_all_movie_ratings = ratings_by_user_id[user_id]; // ratings de outros filmes do mesmo usuário

            for (map<int, double>::iterator this_user_all_movie_ratings_iter = this_user_all_movie_ratings.begin(); this_user_all_movie_ratings_iter != this_user_all_movie_ratings.end(); this_user_all_movie_ratings_iter++) {

                int movie_b_id = this_user_all_movie_ratings_iter->first;

                if (movie_a_id != movie_b_id) {
                    send_calc_similarity(movie_a_id, movie_b_id, current_process);
                }
            }
        }
    }
    all_sent = true;
}

void send_calc_similarity(int movie_a_id, int movie_b_id, int &current_process) {

    bool not_calculated_yet;
    not_calculated_yet = (similarity_task_sent.find(make_pair(movie_a_id, movie_b_id)) == similarity_task_sent.end());

    if (not_calculated_yet) {
        do_send_calc_similarity(movie_a_id, movie_b_id, current_process);
        similarity_task_sent.insert(make_pair(movie_a_id, movie_b_id));
        similarity_task_sent.insert(make_pair(movie_b_id, movie_a_id));
        similarities_sent++;
        current_process++;
        if (current_process > (process_count - 1)) current_process = 1;
    }
}

void do_send_calc_similarity(int movie_a_id, int movie_b_id, int current_process) {

    if (process_movie_id_sent.find(make_pair(current_process, movie_a_id)) == process_movie_id_sent.end())
        send_movie_ratings(movie_a_id, current_process);

    if (process_movie_id_sent.find(make_pair(current_process, movie_b_id)) == process_movie_id_sent.end())
        send_movie_ratings(movie_b_id, current_process);

    int command[] = {movie_a_id, movie_b_id};
    int message_type = 2;
#pragma omp critical(comm)
    {
        MPI_Send(&message_type, 1, MPI_INT, current_process, 0, MPI_COMM_WORLD);    // message type
        MPI_Send(command, 2, MPI_INT, current_process, 0, MPI_COMM_WORLD);          // send movie a
    }
}

void send_movie_ratings(int movie_id, int current_process) {
    movie_ratings_t* movie_ratings = get_structure_rating_for_movie(movie_id);
    int message_type = 1;
#pragma omp critical(comm)
    {
        MPI_Send(&message_type, 1, MPI_INT, current_process, 0, MPI_COMM_WORLD);
        MPI_Send(&movie_id, 1, MPI_INT, current_process, 0, MPI_COMM_WORLD); // movie
        MPI_Send(&(movie_ratings->number_of_ratings), 1, MPI_INT, current_process, 0, MPI_COMM_WORLD); // send size
        MPI_Send(movie_ratings->user_ids, movie_ratings->number_of_ratings, MPI_INT, current_process, 0, MPI_COMM_WORLD); // send ids
        MPI_Send(movie_ratings->ratings, movie_ratings->number_of_ratings, MPI_DOUBLE, current_process, 0, MPI_COMM_WORLD); // send ratings
    }

    process_movie_id_sent.insert(make_pair(current_process, movie_id));
}

movie_ratings_t * get_structure_rating_for_movie(int movie_id) {

    if (struct_ratings_for_movie.find(movie_id) == struct_ratings_for_movie.end()) { // não fez pra esse filme ainda
        movie_ratings_t * r = new movie_ratings_t;
        r->number_of_ratings = 0;
        if (ratings_by_movie_id.find(movie_id) != ratings_by_movie_id.end()) { // tem ratings para esse filme
            map<int, double> & this_movie_ratings = ratings_by_movie_id[movie_id];  //cria um alias
            int size = (int) this_movie_ratings.size();
            r->number_of_ratings = size;
            r->user_ids = new int[size];
            r->ratings = new double[size];
            int index = 0;
            for (map<int, double>::iterator iter = this_movie_ratings.begin(); iter != this_movie_ratings.end(); iter++) {
                r->user_ids[index] = iter->first;
                r->ratings[index] = iter->second;
                index++;
            }
        }
        struct_ratings_for_movie[movie_id] = r;
    }
    return struct_ratings_for_movie[movie_id];
}

void main_process_destroy() {
    for (map<int, movie_ratings_t*>::iterator iter = struct_ratings_for_movie.begin(); iter != struct_ratings_for_movie.end(); iter++) {
        movie_ratings_t* x = iter->second;
        if (x->number_of_ratings > 0) {
            delete[] x->user_ids;
            delete[] x->ratings;
            delete x;
        }
    }
}

void receive_result() {
    bool finished = false;
    while (!finished) {
        double response[3];
        int msg_avail;
        MPI_Status status;
#pragma omp critical(comm)
        MPI_Iprobe(MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &msg_avail, &status);
        if (msg_avail) {
#pragma omp critical(comm)
            MPI_Recv(&response[0], 3, MPI_DOUBLE, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            similarities[response[0]][response[1]] = response[2];
            similarities[response[1]][response[0]] = response[2];
            similarities_received++;
        }
        finished = all_sent && (similarities_received == similarities_sent);
    }
    kill_workers();
}

void kill_workers() {
    int message_type = 3;
    int process;
    for (process = 1; process < process_count; process++) {
        MPI_Send(&message_type, 1, MPI_INT, process, 0, MPI_COMM_WORLD); // message type
    }
}

bool cmp_sims(pair<int, double> a, pair<int, double> b) {
    return a.second > b.second;
}

void show_similar_movies(int movie_id) {

    if (similarities.find(movie_id) != similarities.end()) {
        map<int, double> &sims = similarities[movie_id];

        vector<pair<int,double> > simvec(sims.begin(), sims.end());

        sort(simvec.begin(), simvec.end(), cmp_sims); // ordena por similaridade

        cout << "Filmes similares a: " << movie_names[movie_id] << endl << endl;
        int i=0;
        for (vector<pair<int, double> >::iterator iter = simvec.begin(); i<10 && iter != simvec.end(); iter++) {
            int sim_movie_id = iter->first;
            double similarity = iter->second;
            cout << movie_names[sim_movie_id] << " : " << similarity << endl;
            i++;
        }
    }
}


/*
*
* WORKER PROCESSES
*
* */
void worker_process() {

    while (true) {
        int message_type;
        MPI_Recv(&message_type, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        if (message_type == 1) {
            receive_ratings();
        }else if (message_type == 2) {
            receive_calc_sim();
        } else if (message_type == 3) {
            break;
        }
    }
}

void receive_ratings() {

    int movie_id;
    MPI_Recv(&movie_id, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    int size;
    MPI_Recv(&size, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    int *ids = new int[size];
    MPI_Recv(ids, size, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    double *ratings = new double[size];
    MPI_Recv(ratings, size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    int i;
    for (i = 0; i < size; i++) my_ratings[movie_id][ids[i]] = ratings[i];

    delete[] ids;
    delete[] ratings;
}

void receive_calc_sim() {
    int cmd[2];
    MPI_Recv(cmd, 2, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    double response[3];
    response[0] = cmd[0];
    response[1] = cmd[1];
    response[2] = calc_sim(cmd[0], cmd[1]);
    MPI_Send(&response[0], 3, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
}

double calc_sim(int movie_a_id, int movie_b_id) {

    double sum_of_squares = 0;

    map<int, double> &movie_a_ratings = my_ratings[movie_a_id]; //alias
    map<int, double> &movie_b_ratings = my_ratings[movie_b_id]; //alias

    for (map<int, double>::iterator iter = movie_a_ratings.begin(); iter != movie_a_ratings.end(); iter++) { // para cada rating to filme A

        int user_id = iter->first;
        double rating_a = iter->second;

        if (movie_b_ratings.find(user_id) != movie_b_ratings.end()) { // verifica se existe rating do mesmo usuário para filme B
            double rating_b = movie_b_ratings[user_id];
            sum_of_squares += pow(rating_a - rating_b, 2);
        }
    }

    return 1 / (1 + sqrt(sum_of_squares));
}


int main() {
    go();
    return 0;
}